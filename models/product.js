var sql = require("../config/db");

module.exports.save_product = (product, callback) => {
  sql.query("INSERT INTO product set ?", product, callback);
};

module.exports.save_category = (product,callback) =>{
    sql.query("INSERT INTO product_categories set ?", product, callback);
}

module.exports.get_all_products = (callback) => {
  sql.query(
    "SELECT * FROM product INNER JOIN product_categories ON product.type=product_categories.category_id",
    callback
  );
};

module.exports.get_by_id = (id, callback) => {
  sql.query(
    "SELECT * FROM product INNER JOIN product_categories ON product.type=product_categories.category_id WHERE product.id ='" + id + "'",
    callback
  );
};

module.exports.get_by_category = (id, callback) => {
  sql.query("SELECT * FROM product WHERE product.type ='" + id + "'", callback);
};

module.exports.get_all_categories = (callback) => {
  sql.query("SELECT * FROM product_categories", callback);
};
