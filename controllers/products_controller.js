const express = require("express");
var router = express.Router();
const passport = require("passport");
const { check, validationResult } = require("express-validator");
var Proudct = require("../models/product");

router.post(
  "/add",
  passport.authenticate("jwt", { session: false }),
  [
    check("name")
      .exists()
      .isString()
      .withMessage("Must be alphabetical Chars"),
    check("quantity")
      .exists()
      .isNumeric()
      .withMessage("Must be a Number"),
    check("price")
      .exists()
      .isString()
      .matches(/^[1-9]\d*(((,\d{3}){1})?(\.\d{0,2})?)$/)
      .withMessage("Must be something like 120.40"),
    check("color").isString().optional(),
    check("description").exists().isString().withMessage("Required Feild"),
    check("type")
      .exists()
      .isNumeric()
      .withMessage("Required Feild and Should be a number"),
    check("image").isString().optional(),
  ],
  (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    } else {
      var product = {
        name: req.body.name,
        price: req.body.price,
        color: req.body.color || null,
        description: req.body.description,
        type: req.body.type,
        image: req.body.image || null,
        quantity: req.body.quantity,
      };

      Proudct.save_product(product, (err, prod) => {
        if (!err) {
          res.status(200).json({
            status: true,
            msg: "Succesfully Added!",
            data: prod,
          });
        } else {
          res
            .status(500)
            .json({
              state: false,
              msg: "Something went wrong please try again!",
              data: JSON.stringify(err),
            });
        }
      });
    }
  }
);

router.post("/addcategory",
    passport.authenticate("jwt", { session: false }),
    [
      check("name")
        .exists()
        .isString()
        .withMessage("Must be alphabetical Chars"),
      check("description").isString().optional(),
    ],
    (req, res) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() });
      } else {
        var product = {
          category_name: req.body.name,
          discription: req.body.description || null,
        };
  
        Proudct.save_category(product, (err, prod) => {
          if (!err) {
            res.status(200).json({
              status: true,
              msg: "Succesfully Added!",
              data: prod,
            });
          } else {
            res
              .status(500)
              .json({
                state: false,
                msg: "Something went wrong please try again!",
                data: JSON.stringify(err),
              });
          }
        });
      }
    }
  );

router.get("/all", (req, res) => {
  Proudct.get_all_products((err, products) => {
    if (!err) {
      res.status(200).json({
        status: true,
        msg: "Reading all products",
        data: products,
      });
    } else {
      res
        .status(500)
        .json({
          state: false,
          msg: "Something went wrong please try again!",
          data: JSON.stringify(err),
        });
    }
  });
});

router.get("/getbyid", (req, res) => {
    var id = req.query.id
  Proudct.get_by_id(id, (err, product) => {
    if (!err) {
      res.status(200).json({
        status: true,
        msg: "Reading product by id",
        data: product,
      });
    } else {
      res
        .status(500)
        .json({
          state: false,
          msg: "Something went wrong please try again!",
          data: JSON.stringify(err),
        });
    }
  });
});

router.get("/getbycategory",(req,res) =>{
    var cat_id = req.query.id;
    Proudct.get_by_category(cat_id, (err, products) => {
        if (!err) {
          res.status(200).json({
            status: true,
            msg: "Reading all products by category",
            data: products,
          });
        } else {
          res
            .status(500)
            .json({
              state: false,
              msg: "Something went wrong please try again!",
              data: JSON.stringify(err),
            });
        }
      });
})

router.get("/allcategories", (req, res) => {
    Proudct.get_all_categories((err, categories) => {
      if (!err) {
        res.status(200).json({
          status: true,
          msg: "Reading all products",
          data: categories,
        });
      } else {
        res
          .status(500)
          .json({
            state: false,
            msg: "Something went wrong please try again!",
            data: JSON.stringify(err),
          });
      }
    });
  });



module.exports = router;
